using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionScreen : MonoBehaviour
{
    public GameObject instructionScreen;
    public GameObject menuScreen;
    public GameObject creditScreen;
    public void OpenInstructions()
    {
        instructionScreen.SetActive(true);
        menuScreen.SetActive(false);
    }
    public void CloseInstructions()
    {
        instructionScreen.SetActive(false);
        menuScreen.SetActive(true);
    }
    public void OpenCreditScreen()
    {
        creditScreen.SetActive(true);
        menuScreen.SetActive(false);
    }
    public void CloseCreditScreen()
    {
        creditScreen.SetActive(false);
        menuScreen.SetActive(true);
    }
}
