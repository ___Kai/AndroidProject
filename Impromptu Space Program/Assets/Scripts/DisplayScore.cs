using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DisplayScore : MonoBehaviour
{
    public LaunchControls lC;
    public GameObject uIObject;
    public TMP_Text scoreText;
    public float score;
    public bool atMaxHeight;
    // Start is called before the first frame update
    void Start()
    {
        uIObject.SetActive(false);
        atMaxHeight = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(atMaxHeight)
        {
            score = lC.highestPoint;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        uIObject.SetActive(true);
        scoreText.text = "LAUNCH COMPLETED. \n HEIGHT ACHIEVED: \n" + score + " metres.";
    }
}

