using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketSelection : MonoBehaviour
{
    public List<GameObject> rockets = new List<GameObject>();

    public void SelectRocket(int avaliableRockets)
    {
        StaticManager.selectedRocket = avaliableRockets;
    }
}
