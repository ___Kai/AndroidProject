using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRocket : MonoBehaviour
{
    public Transform sP;
    public StaticManager manager;
    public RocketSelection rocketSelection;
    // Start is called before the first frame update
    void Start()
    {
        //rocketSelection.rockets = StaticManager.selectedRocket;
        //Instantiate the roecket correspoding to the value of selectedRocket

        int boop = manager.RocketNumber();

        Instantiate(rocketSelection.rockets[boop], sP);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
